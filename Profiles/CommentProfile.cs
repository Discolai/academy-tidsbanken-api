﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.Comment;
using academy_tidsbanken_api.Models;
using AutoMapper;

namespace academy_tidsbanken_api.Profiles
{
    public class CommentProfile : Profile
    {
        public CommentProfile()
        {
            CreateMap<Comment, CommentViewDto>()
                .ForMember(dest => dest.UserName, src => src.MapFrom(c => c.User.ToString()));

            CreateMap<CommentCreateDto, Comment>();
            CreateMap<CommentUpdateDto, Comment>();

        }
    }
}
