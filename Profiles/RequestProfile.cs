﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.Request;
using academy_tidsbanken_api.Models;
using AutoMapper;

namespace academy_tidsbanken_api.Profiles
{
    public class RequestProfile : Profile
    {
        public RequestProfile()
        {
            CreateMap<Request, RequestViewDto>()
            .ForMember(dest => dest.OwnerName, src => src.MapFrom(r => r.Owner.ToString()))
            .ForMember(dest => dest.ModeratorName, src => src.MapFrom(r => r.Moderator == null ? null : r.Moderator.ToString()));

            CreateMap<RequestCreateDto, Request>().ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));
            CreateMap<RequestUpdateDto, Request>().ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));
        }
    }
}
