﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.User;
using academy_tidsbanken_api.Models;
using AutoMapper;

namespace academy_tidsbanken_api.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserFullViewDto>().ForMember(dest => dest.Name, src => src.MapFrom(u => u.ToString()));
            CreateMap<User, UserLimitedViewDto>().ForMember(dest => dest.Name, src => src.MapFrom(u => u.ToString()));

            CreateMap<UserCreateDto, User>().ReverseMap();
            CreateMap<UserUpdateDto, User>().ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));

        }
    }
}
