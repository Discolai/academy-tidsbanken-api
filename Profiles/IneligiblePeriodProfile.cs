﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.IneligiblePeriods;
using academy_tidsbanken_api.Models;
using AutoMapper;

namespace academy_tidsbanken_api.Profiles
{
    public class IneligiblePeriodProfile: Profile
    {
        public IneligiblePeriodProfile() {
            CreateMap<IneligiblePeriodViewDto, IneligiblePeriod>().ReverseMap();
            CreateMap<IneligiblePeriodCreateDto, IneligiblePeriod>();

            CreateMap<IneligiblePeriodUpdateDto, IneligiblePeriod>().ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));

        }
    }
}
