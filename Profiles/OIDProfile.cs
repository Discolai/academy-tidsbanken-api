﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.OID;
using academy_tidsbanken_api.Models.OID;
using AutoMapper;

namespace academy_tidsbanken_api.Profiles
{
    public class OIDProfile : Profile
    {
        public OIDProfile()
        {
            CreateMap<OIDTokens, OIDTokensViewDto>()
                .ForMember(dest => dest.AccessToken, src => src.MapFrom(tokens => tokens.access_token))
                .ForMember(dest => dest.RefreshToken, src => src.MapFrom(tokens => tokens.refresh_token))
                .ForMember(dest => dest.IdToken, src => src.MapFrom(tokens => tokens.id_token));

        }
    }
}
