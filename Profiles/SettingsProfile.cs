﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.Settings;
using academy_tidsbanken_api.Models;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Profiles
{
    public class SettingsProfile : Profile
    {
        public SettingsProfile()
        {
            CreateMap<Setting, SettingsViewDto>().ReverseMap();

            CreateMap<SettingsUpdateDto, Setting>()
                .ForAllMembers(opt => opt.Condition((source, dest, sourceMember, destMember) => sourceMember != null));
        }
    }

    class Resolver : IMemberValueResolver<object, object, object, object>
    {
        public object Resolve(object source, object destination, object sourceMember, object destinationMember, ResolutionContext context)
        {
            return sourceMember ?? destinationMember;
        }
    }
}
