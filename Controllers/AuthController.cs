﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Dtos.Auth;
using academy_tidsbanken_api.Dtos.OID;
using academy_tidsbanken_api.Models.OID;
using academy_tidsbanken_api.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Swashbuckle.AspNetCore.Annotations;

namespace academy_tidsbanken_api.Controllers
{
    [Route("/")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly OIDService _oIDService;
        private readonly IMapper _mapper;
        private readonly ILogger<AuthController> _logger;
        private readonly UserService _userService;
        public AuthController(OIDService oIDService, IMapper mapper, ILogger<AuthController> logger, UserService userService)
        {
            _oIDService = oIDService;
            _mapper = mapper;
            _logger = logger;
            _userService = userService;
        }


        /// <summary>
        /// Authenticates a user against a keycloak identity server
        /// </summary>
        /// <remarks>
        /// Creates a user if it does not exist in the database and authenticates successfully
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("/login")]
        public async Task<ObjectResult> LoginAsync(LoginDto loginData)
        {
            // Validate grant type
            switch (loginData.GrantType)
            {
                case GrantTypes.Password:
                    if (String.IsNullOrEmpty(loginData.Username))
                    {
                        _logger.LogInformation("username");
                        ModelState.AddModelError("Username", "Please specify a username");
                    }
                    if (String.IsNullOrEmpty(loginData.Password))
                    {
                        _logger.LogInformation("password");
                        ModelState.AddModelError("Password", "Please specify a password");
                    }
                    break;
                case GrantTypes.Token:
                    if (String.IsNullOrEmpty(loginData.RefreshToken))
                    {
                        ModelState.AddModelError("RefreshToken", "Please specify a refreshtoken");
                    }
                    break;
                default:
                    if (String.IsNullOrEmpty(loginData.RefreshToken))
                    {
                        ModelState.AddModelError("GrantType", "Please specify a valid grant type, 'password' or 'refresh_token'");
                    }
                    break;
            }
            if (!ModelState.IsValid)
            {
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }


            Dictionary<string, string[]> errors = null;
            OIDTokens tokens = await _oIDService.LoginAsync(loginData);
            if (tokens == null)
            {
                var error = loginData.GrantType switch
                {
                    GrantTypes.Password => new[] { "Invalid username or password" },
                    GrantTypes.Token => new[] { "Invalid refresh token" },
                    _ => throw new NotSupportedException(loginData.GrantType)
                };

                errors = new Dictionary<string, string[]> { { "",  error} };
            }
            if (tokens != null)
            {
                // Parse token and create user if it does not exist in the database
                var idToken = new JwtSecurityTokenHandler().ReadJwtToken(tokens.id_token);
                var user = await _userService.GetByIdAsync(idToken.Claims.FirstOrDefault(c => c.Type == "sub")?.Value);
                if (user == null)
                {
                    user = new Models.User
                    {
                        Id = idToken.Claims.FirstOrDefault(c => c.Type == "sub")?.Value,
                        Email = idToken.Claims.FirstOrDefault(c => c.Type == "email")?.Value,
                        FirstName = idToken.Claims.FirstOrDefault(c => c.Type == "given_name")?.Value,
                        LastName = idToken.Claims.FirstOrDefault(c => c.Type == "family_name")?.Value,
                        IsAdmin = (bool)(idToken.Claims.FirstOrDefault(c => c.Type == "roles")?.Value.Contains("admin"))
                    };
                    await _userService.SaveUserAsync(user);
                }
            }



            var data = tokens == null ? null : _mapper.Map<OIDTokensViewDto>(tokens);

            return CommonResponse.WithErrors(data, errors, errors == null ? 200 : 401);
        }

    }
}
