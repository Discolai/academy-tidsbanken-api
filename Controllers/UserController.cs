﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Dtos.Auth;
using academy_tidsbanken_api.Dtos.Request;
using academy_tidsbanken_api.Dtos.User;
using academy_tidsbanken_api.Models;
using academy_tidsbanken_api.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;


namespace academy_tidsbanken_api.Controllers
{
    [Authorize]
    [Route("/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly UserService _userService;
        private readonly OIDService _oIDService;
        private readonly IMapper _mapper;
        private readonly RequestService _requestService;



        public UserController(ILogger<UserController> logger, UserService userService, OIDService oIDService, IMapper mapper, RequestService requestService)
        {
            _logger = logger;
            _userService = userService;
            _oIDService = oIDService;
            _mapper = mapper;
            _requestService = requestService;
        }


        /// <summary>
        /// Get the user details link
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [SwaggerResponse(303)]
        [SwaggerResponseHeader(303, "Location", "string", "The location of the user's details page.")]
        [Produces("application/json")]
        [HttpGet()]
        public async Task<ObjectResult> GetUserOwnUrl()
        {
            var user = await _userService.CurrentUserAsync();
            if (user == null) return CommonResponse.Create(status: 404);


            var location = $"{HttpContext.Request.GetDisplayUrl()}/{user.Id}";

            HttpContext.Response.Headers.Add("Location", location);
            return CommonResponse.Create(new Dictionary<string, string> { { "Location", location } }, status: 303);
        }

        /// <summary>
        /// Get all users.
        /// </summary>
        /// <remarks>
        /// Admins get the full representation of all users.
        /// Other users get the limited representation of all users.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet("all")]
        public async Task<ObjectResult> GetAllAsync([FromQuery]UserQueryParams parameters)
        {
            var users = await _userService.GetAllAsync(parameters);
            if (_userService.CurrentUserIsAdmin) return CommonResponse.Create(_mapper.Map<UserFullViewDto[]>(users));
            return CommonResponse.Create(_mapper.Map<UserLimitedViewDto[]>(users));
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ObjectResult> CreateUserAsync(UserCreateDto createDto)
        {
            var user = await _userService.GetByEmailAsync(createDto.Email);
            if (user != null)
            {
                ModelState.AddModelError("Email", "The email has already been registered");
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            user = await _oIDService.CreateRemoteUser(createDto);
            if (user == null)
            {
                return CommonResponse.Create(status: 403);
            }
            await _userService.SaveUserAsync(user);

            return CommonResponse.Create(_mapper.Map<UserFullViewDto>(user), status: 201);
        }

        /// <summary>
        /// Get a single user.
        /// </summary>
        /// <remarks>
        /// Admins get the full representation of the user.
        /// Other users get the limited representation of the user.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet("{userId}")]
        public async Task<ObjectResult> GetUserById(string userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (_userService.CurrentUserIsAdmin || (userId == _userService.CurrentUserId))
            {
                return CommonResponse.Create(_mapper.Map<UserFullViewDto>(user));
            }
            return CommonResponse.Create(_mapper.Map<UserLimitedViewDto>(user));
        }


        /// <summary>
        /// Update a user.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the user itself.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPatch("{userId}")]
        public async Task<ObjectResult> UpdateUserByIdAsync(string userId, UserUpdateDto updateDto)
        {
            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != userId)
            {
                ModelState.AddModelError("", "Only admins can edit other users accounts");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (!_userService.CurrentUserIsAdmin && updateDto.IsAdmin != null)
            {
                ModelState.AddModelError("IsAdmin", "Only admins can set the admin state of a user");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            if (!await _oIDService.UpdateRemoteUserAsync(userId, updateDto))
            {
                _logger.LogError("Failed updating remote user");
                return CommonResponse.Create(status: 403);
            }

            user = _mapper.Map(updateDto, user);
            user = await _userService.UpdateAsync(user);

            return CommonResponse.Create(_mapper.Map<UserFullViewDto>(user));
        }

        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the user itself.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpDelete("{userId}")]
        public async Task<ObjectResult> DeleteUserByIdAsync(string userId)
        {
            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != userId)
            {
                ModelState.AddModelError("", "Only admins can delete other users accounts");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            if (await _oIDService.DeleteRemoteUserAsync(userId))
            {
                var user = await _userService.DeleteById(userId);
                
                // Return the deleted user as a precaution
                return CommonResponse.Create(_mapper.Map<UserCreateDto>(user));
            }
            else
            {
                return CommonResponse.Create(status: 403);
            }

        }


        /// <summary>
        /// Get requests written by a user.
        /// </summary>
        /// <remarks>
        /// Admins and the user itself can view all requests, other users can only view approved requests.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpGet("{userId}/requests")]
        public async Task<ObjectResult> GetUserRequestsAsync(string userId, [FromQuery]RequestQueryParams parameters)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
            {
                return CommonResponse.Create(status: 404);
            }

            var requests = (_userService.CurrentUserIsAdmin || _userService.CurrentUserId == userId) switch
            {
                true => _mapper.Map<RequestViewDto[]>(await _requestService.GetByOwnerAsync(parameters, user)),
                false => _mapper.Map<RequestViewDto[]>(await _requestService.GetByOwnerAsync(parameters, user, RequestStates.Approved))
            };
            return CommonResponse.Create(requests);
        }


        /// <summary>
        /// Update a user's password.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the user itself.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("{userId}/reset-password")]
        public async Task<ObjectResult> UpdateUserPasswordAsync(string userId, UserResetPasswordDto passwordDto)
        {
            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != userId)
            {
                ModelState.AddModelError("", "Only admins can reset the password of other users accounts");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            var user = _userService.GetByIdAsync(userId);
            if (user == null) return CommonResponse.Create(status: 404);

            if (passwordDto.Password1 != passwordDto.Password2)
            {
                ModelState.AddModelError("Password1", "Passwords must match");
                ModelState.AddModelError("Password2", "Passwords must match");
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            if (!await _oIDService.UpdateRemoteUserPassword(userId, passwordDto.Password1))
            {
                _logger.LogError("Failed updating remote user password");
                return CommonResponse.Create(status: 500);
            }

            return CommonResponse.Create();
        }

        /// <summary>
        /// Send a password reset mail
        /// </summary>
        /// <remarks>
        /// Will return 200 ok even if the email does not exist
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [AllowAnonymous]
        [HttpPost("forgot-password")]
        public async Task<ObjectResult> ForgotPasswordAsync(UserForgotPasswordDto dto)
        {
            var user = await _userService.GetByEmailAsync(dto.Email);

            // Return 200 even if the user doesnt exist as a security precaution
            if (user == null) return CommonResponse.Create();

            await _oIDService.RemoteExecuteActionsEmailAsync(user.Id, EmailActions.UpdatePassword);
            return CommonResponse.Create();
        }
    }

    
}
