﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Dtos.Settings;
using academy_tidsbanken_api.Models;
using academy_tidsbanken_api.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace academy_tidsbanken_api.Controllers
{
    [Route("/[controller]")]
    [Authorize]
    [ApiController]
    public class SettingsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly SettingsService _settingsService;

        public SettingsController(IMapper mapper, SettingsService settingsService)
        {
            _mapper = mapper;
            _settingsService = settingsService;
        }

        /// <summary>
        /// Get settings
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet]
        public async Task<ObjectResult> getAll()
        {
            return CommonResponse.Create(_mapper.Map<SettingsViewDto>(await _settingsService.GetAsync()));
        }

        /// <summary>
        /// Update settings.
        /// </summary>
        /// <remarks>
        /// Restricted to admins.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [Authorize(Roles = "admin")]
        [HttpPut]
        public async Task<ObjectResult> modifySettings(SettingsUpdateDto updateDto)
        {
            var settings = await _settingsService.UpateSettingsAsync(updateDto);
            return CommonResponse.Create(_mapper.Map<SettingsViewDto>(settings));
        }
    }
}
