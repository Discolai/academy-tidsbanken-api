﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using academy_tidsbanken_api.Data;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query.Internal;
using academy_tidsbanken_api.Models;
using Microsoft.EntityFrameworkCore;
using academy_tidsbanken_api.Dtos.IneligiblePeriods;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Services;
using Swashbuckle.AspNetCore.Annotations;

namespace academy_tidsbanken_api.Controllers
{
    [Authorize]
    [Route("/ineligible")]
    [ApiController]
    public class IneligiblePeriodController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IneligiblePeriodService _ineligiblePeriodService;
        private readonly UserService _userService;

        public IneligiblePeriodController(
            IMapper mapper, 
            IneligiblePeriodService ineligiblePeriodService, 
            UserService userService)
        {
            _mapper = mapper;
            _ineligiblePeriodService = ineligiblePeriodService;
            _userService = userService;
        }

        private bool ValidateIneligiblePeriodObject(IneligiblePeriod ip)
        {
            if (ip.End < ip.Start)
            {
                ModelState.AddModelError("Start", "The start date must come before the end date");
            }
            return ModelState.IsValid;
        }

        /// <summary>
        /// Get all ineligible periods.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet]
        public async Task<ObjectResult> GetAllAsync([FromQuery]IneligiblePeriodQueryParams parameters)
        {
            var ips = await _ineligiblePeriodService.GetAllAsync(parameters);
            return CommonResponse.Create(_mapper.Map<IneligiblePeriodViewDto[]>(ips));

        }

        /// <summary>
        /// Creates a new ineligible period.
        /// </summary>
        /// <remarks>
        /// Restricted to admins.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ObjectResult> CreateAsync(IneligiblePeriodCreateDto createDto)
        {
            var ip = _mapper.Map<IneligiblePeriod>(createDto);
            if (!ValidateIneligiblePeriodObject(ip))
            {
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            ip.User = await _userService.CurrentUserAsync();
            await _ineligiblePeriodService.CreateAsync(ip);

            return CommonResponse.Create(_mapper.Map<IneligiblePeriodViewDto>(ip), 201);
        }

        /// <summary>
        /// Get a single ineligible period.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet("{id}")]
        public async Task<ObjectResult> GetByIdAsync(int id)
        {
            var ip = await _ineligiblePeriodService.GetByIdAsync(id);
            if (ip == null)
            {
                return CommonResponse.Create(status: 404);
            }
            return CommonResponse.Create(_mapper.Map<IneligiblePeriodViewDto>(ip));
        }

        /// <summary>
        /// Updates an ineligible period.
        /// </summary>
        /// <remarks>
        /// Restricted to admins.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [Authorize(Roles = "admin")]
        [HttpPatch("{id}")]
        public async Task<ObjectResult> UpdateAsync(int id, IneligiblePeriodUpdateDto updateDto)
        {
            var ip = await _ineligiblePeriodService.GetByIdAsync(id);
            if (ip == null)
            {
                return CommonResponse.Create(status: 404);
            }

            ip = _mapper.Map(updateDto, ip);
            if (!ValidateIneligiblePeriodObject(ip))
            {
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            ip = await _ineligiblePeriodService.UpdateAsync(ip);
            return CommonResponse.Create(_mapper.Map<IneligiblePeriodViewDto>(ip));
        }


        /// <summary>
        /// Deletes an ineligible period.
        /// </summary>
        /// <remarks>
        /// Restricted to admins.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public async Task<ObjectResult> DeleteAsync(int id)
        {
            var ip = await _ineligiblePeriodService.GetByIdAsync(id);
            if (ip == null)
            {
                return CommonResponse.Create(status: 404);
            }

            var responseObject = _mapper.Map<IneligiblePeriodViewDto>(ip);
            
            await _ineligiblePeriodService.DeleteAsync(ip);
            return CommonResponse.Create(responseObject);
        }
    }
}
