﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Dtos.IneligiblePeriods;
using academy_tidsbanken_api.Dtos.Request;
using academy_tidsbanken_api.Models;
using academy_tidsbanken_api.Services;
using AutoMapper;
using FluentDateTime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace academy_tidsbanken_api.Controllers
{
    [Route("/[Controller]")]
    [Authorize]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private readonly RequestService _requestService;
        private readonly UserService _userService;
        private readonly IMapper _mapper;
        private readonly SettingsService _settingsService;
        private readonly IneligiblePeriodService _ineligiblePeriodService;

        public RequestController(
            RequestService requestService, 
            UserService userService, 
            IMapper mapper, 
            SettingsService settingsService,
            IneligiblePeriodService ineligiblePeriodService)
        {
            _requestService = requestService;
            _userService = userService;
            _mapper = mapper;
            _settingsService = settingsService;
            _ineligiblePeriodService = ineligiblePeriodService;
        }

        private async Task<bool> ValidateRequestObject(Request request)
        {
            if (request.PeriodStart > request.PeriodEnd)
            {
                ModelState.AddModelError("PeriodStart", "The start date must come before the end date");
            }
            var settings = await _settingsService.GetAsync();
            if (request.PeriodStart.Value.AddBusinessDays((int)settings.MaxVacationLength) < request.PeriodEnd)
            {
                ModelState.AddModelError("PeriodStart", $"You cannot request time off for more than {settings.MaxVacationLength} business days.");
            }

            var ineligible = await _ineligiblePeriodService.GetAllAsync(new IneligiblePeriodQueryParams { Start = request.PeriodStart.Value, End = request.PeriodEnd.Value });
            if (ineligible.Count() > 0)
            {
                ModelState.AddModelError("PeriodStart", "The selected period is ineligible");
            }

            if (!RequestStates.IsValid(request.State))
            {
                var states = RequestStates.GetStates();
                ModelState.AddModelError("State", $"Please specify a valid state: {string.Join(", ", states.Take(states.Count() - 1))} or {states.Last()}");
            }
            return ModelState.IsValid;
        }

        /// <summary>
        /// Get all requests.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet]
        public async Task<ObjectResult> GetAllAsync([FromQuery]RequestQueryParams parameters)
        {
            var requests = _userService.CurrentUserIsAdmin switch
            {
                true => _mapper.Map<RequestViewDto[]>(await _requestService.GetAllAsync(parameters)),
                false => _mapper.Map<RequestViewDto[]>(await _requestService.GetAllLimitByUser(_userService.CurrentUserId, parameters))
            };
            return CommonResponse.Create(requests);
        }

        /// <summary>
        /// Get a single request.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet("{requestId}")]
        public async Task<ObjectResult> GetByIdAsync(int requestId)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null) return CommonResponse.Create(status: 404);

            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != request.OwnerId && request.State != RequestStates.Approved)
            {
                return CommonResponse.Create(status: 403);
            }
            return CommonResponse.Create(_mapper.Map<RequestViewDto>(request));
        }

        /// <summary>
        /// Creates a new request.
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost]
        public async Task<ObjectResult> CreateRequestAsync(RequestCreateDto createDto)
        {
            var request = _mapper.Map<Request>(createDto);
            if (!await ValidateRequestObject(request))
            {
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            request.Owner = await _userService.CurrentUserAsync();
            await _requestService.CreateAsync(request);
            return CommonResponse.Create(_mapper.Map<RequestViewDto>(request), 201);
        }


        /// <summary>
        /// Updates a request.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the request author.
        /// The request author can only edit the request while it is pending.   
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPatch("{requestId}")]
        public async Task<ObjectResult> UpdateRequestAsync(int requestId, RequestUpdateDto updateDto)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return CommonResponse.Create(status: 404);
            }

            // Check if the user owns the request or is an admin
            if (_userService.CurrentUserId != request.OwnerId && !_userService.CurrentUserIsAdmin)
            {
                ModelState.AddModelError("", "Only admins can edit other users requests");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            // Check if the owner tries to update the request state
            if (_userService.CurrentUserId == request.OwnerId && updateDto.State != null)
            {
                ModelState.AddModelError("", "Users cannot change the state of their own requests");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            request = _mapper.Map(updateDto, request);
            if (!await ValidateRequestObject(request))
            {
                return CommonResponse.WithModelState(modelState: ModelState, status: 400);
            }

            request = await _requestService.UpdateAsync(request, _userService.CurrentUserId != request.OwnerId ? await _userService.CurrentUserAsync() : null);

            return CommonResponse.Create(_mapper.Map<RequestViewDto>(request));
        }

        /// <summary>
        /// Deletes a request.
        /// </summary>
        /// <remarks>
        /// Restricted to admins. 
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Authorize(Roles = "admin")]
        [HttpDelete("{requestId}")]
        public async Task<ObjectResult> DeleteRequestAsync(int requestId)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return CommonResponse.Create(status: 404);
            }
            var responseObject = _mapper.Map<RequestViewDto>(request);

            await _requestService.DeleteAsync(request);
            return CommonResponse.Create(responseObject);
        }
    }
}

