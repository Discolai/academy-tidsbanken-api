﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos;
using academy_tidsbanken_api.Dtos.Comment;
using academy_tidsbanken_api.Models;
using academy_tidsbanken_api.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace academy_tidsbanken_api.Controllers
{
    [Authorize]
    [Route("request")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly CommentService _commentService;
        private readonly RequestService _requestService;
        private readonly IMapper _mapper;

        public CommentController(UserService userService, CommentService commentService, RequestService requestService, IMapper mapper)
        {
            _userService = userService;
            _commentService = commentService;
            _requestService = requestService;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all comments associated with the specified request
        /// </summary>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [HttpGet("{requestId}/comment")]
        public async Task<ObjectResult> GetCommentsAsync(int requestId, [FromQuery]CommentQueryParams parameters)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != request.OwnerId)
            {
                ModelState.AddModelError("", "Only admins can view comments from other users requests");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            var comments = await _commentService.GetByRequestIdAsync(requestId, parameters.Offset, parameters.Limit);
            return CommonResponse.Create(_mapper.Map<CommentViewDto[]>(comments));
        }

        /// <summary>
        /// Creates a new comment 
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the request author
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPost("{requestId}/comment")]
        public async Task<ObjectResult> CreateCommentAsync(int requestId, CommentCreateDto createDto)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != request.OwnerId)
            {
                ModelState.AddModelError("", "Only admins can comment on other users requests");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            var comment = _mapper.Map<Comment>(createDto);
            comment.User = await _userService.CurrentUserAsync();
            comment.Request = request;
            await _commentService.SaveAsync(comment);

            return CommonResponse.Create(_mapper.Map<CommentViewDto>(comment), 201);
        }

        /// <summary>
        /// Updates a comment.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the request author.
        /// Comments can not be edited 24 hours after they were created.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpPatch("{requestId}/comment/{commentId}")]
        public async Task<ObjectResult> UpdateCommentAsync(int requestId, int commentId, CommentUpdateDto updateDto)
        {
            var request = await _requestService.GetByIdAsync(requestId);
            if (request == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != request.OwnerId)
            {
                ModelState.AddModelError("", "Only admins can comment on other users requests");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            var comment = await _commentService.GetByRequestAndCommentIdAsync(requestId, commentId);
            if (comment == null)
            {
                return CommonResponse.Create(status: 404);
            }

            if (comment.CreatedOn.AddDays(1) < DateTime.UtcNow)
            {
                ModelState.AddModelError("", "You cannot edit comments after 24 hours");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            comment = _mapper.Map(updateDto, comment);
            comment = await _commentService.UpdateAsync(comment);
            return CommonResponse.Create(_mapper.Map<CommentViewDto>(comment));
        }

        /// <summary>
        /// Deletes a comment.
        /// </summary>
        /// <remarks>
        /// Restricted to admins and the request author.
        /// Comments can not be deleted 24 hours after they were created.
        /// </remarks>
        [ProducesDefaultResponseType(typeof(CommonResponse))]
        [Produces("application/json")]
        [Consumes("application/json")]
        [HttpDelete("{requestId}/comment/{commentId}")]
        public async Task<ObjectResult> DeleteCommentAsync(int requestId, int commentId)
        {
            var comment = await _commentService.GetByRequestAndCommentIdAsync(requestId, commentId);
            if (comment == null)
            {
                return CommonResponse.Create(status: 404);
            }
            
            if (!_userService.CurrentUserIsAdmin && _userService.CurrentUserId != comment.UserId)
            {
                ModelState.AddModelError("", "Only admins can delete other users comments");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            if (comment.CreatedOn.AddDays(1) < DateTime.UtcNow)
            {
                ModelState.AddModelError("", "You cannot delete comments after 24 hours");
                return CommonResponse.WithModelState(modelState: ModelState, status: 403);
            }

            await _commentService.DeleteAsync(comment);
            return CommonResponse.Create(_mapper.Map<CommentViewDto>(comment));
        }
    }
}
