﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Extensions
{
    public static class UriExtension
    {
        public static string WithRelative(this Uri baseUri, params string[] relUrls)
        {
            return $"{baseUri.AbsoluteUri}/{string.Join("/", relUrls.Select(u => u.TrimStart('/').TrimEnd('/')))}";
        }
    }
}
