﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace academy_tidsbanken_api.Extensions.IpRateLimit
{
    public static class MiddlewareExtension
    {
        public static IApplicationBuilder UseTidsbankenIpRateLimiting(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TidsbankenIpRateMiddleware>();
        }
    }
}
