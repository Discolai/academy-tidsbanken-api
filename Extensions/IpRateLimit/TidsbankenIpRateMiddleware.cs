﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos;
using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace academy_tidsbanken_api.Extensions.IpRateLimit
{
    public class TidsbankenIpRateMiddleware : IpRateLimitMiddleware
    {
        private readonly IpRateLimitOptions _options;

        public TidsbankenIpRateMiddleware(RequestDelegate next,
            IOptions<IpRateLimitOptions> options,
            IRateLimitCounterStore counterStore,
            IIpPolicyStore policyStore,
            IRateLimitConfiguration config,
            ILogger<TidsbankenIpRateMiddleware> logger)
        : base(next, options, counterStore, policyStore, config, logger) 
        {
            _options = options?.Value;
        }

        public override Task ReturnQuotaExceededResponse(HttpContext httpContext, RateLimitRule rule, string retryAfter)
        {
            var message = string.Format(
                _options.QuotaExceededResponse?.Content ??
                _options.QuotaExceededMessage ??
                "API calls quota exceeded! maximum admitted {0} per {1}.", rule.Limit, rule.Period, retryAfter);

            if (!_options.DisableRateLimitHeaders)
            {
                httpContext.Response.Headers["Retry-After"] = retryAfter;
            }

            httpContext.Response.StatusCode = _options.QuotaExceededResponse?.StatusCode ?? _options.HttpStatusCode;
            httpContext.Response.ContentType = "application/json";

            var json = JsonConvert.SerializeObject(new CommonResponse { Status = httpContext.Response.StatusCode, Message = message });
            return httpContext.Response.WriteAsync(json);
        }

    }
}
