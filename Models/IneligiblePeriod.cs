﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Models
{
    public class IneligiblePeriod
    {
        //id, start, end
        public int Id { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }
        public User User { get; set; }

    }
}
