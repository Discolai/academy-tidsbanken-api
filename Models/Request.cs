﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Newtonsoft.Json;

namespace academy_tidsbanken_api.Models
{
    public static class RequestStates
    {
        public const string Pending = "Pending";
        public const string Approved = "Approved";
        public const string Denied = "Denied";

        private static IEnumerable<System.Reflection.FieldInfo> StateFields()
        {
            return typeof(RequestStates).GetFields();
        }

        public static string[] GetStates()
        {
            return StateFields().Select(s => (string)s.GetRawConstantValue()).ToArray();
        }

        public static bool IsValid(string state)
        {
            return StateFields().Any(f => (string)f.GetRawConstantValue() == state);
        }
    }

    public class Request
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        
        [Required]
        public DateTime? PeriodStart { get; set; }
        [Required]
        public DateTime? PeriodEnd { get; set; }
        [Required]
        [StringLength(10)]
        public string State { get; set; } = RequestStates.Pending;
        
        [Required]
        [StringLength(50)]
        public string OwnerId { get; set; }
        public User Owner { get; set; }

        public DateTime? ModeratedAt { get; set; }
        [StringLength(50)]
        public string ModeratorId { get; set; }
        public User Moderator { get; set; }
        
        public IList<Comment> Comments { get; set; }

        public Request Clone() => (Request)MemberwiseClone();
    }
}
