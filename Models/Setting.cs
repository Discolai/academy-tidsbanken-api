﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Models
{
    /// <summary>
    /// Setting represents a row of the Settings table. There should only ever be
    /// one row in the Settings table.
    /// </summary>
    public class Setting
    {
        // Id is here because I have not found a way to make a true single-row table without a pk
        public int Id { get; set; }
        [Required]
        public int? MaxVacationLength { get; set; }
    }
}
