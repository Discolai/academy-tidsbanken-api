﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Models
{
    public class Comment
    {
        public int Id {get; set;}

        [StringLength(512)]
        [Required]
        public string Message { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime? UpdatedOn { get; set; }
        [Required]
        public int RequestId { get; set; }
        public Request Request { get; set; }
        [Required]
        [StringLength(50)]
        public string UserId { get; set; }
        public User User { get; set; }
    }
}
