﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Models
{
    public class User
    {
        [StringLength(50)]
        public string Id{ get; set; }

        [Required]
        public bool? IsAdmin { get; set; } = false;

        [EmailAddress]
        [Required]
        [StringLength(320)]
        public string Email { get; set; }

        [StringLength(60)]
        public string Picture { get; set; }
        
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        public IList<Request> SubmittedRequests { get; set; }
        public IList<Request> ModeratedRequests { get; set; }
        public IList<Comment> Comments { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
