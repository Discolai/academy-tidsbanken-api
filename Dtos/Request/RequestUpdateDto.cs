﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Models;

namespace academy_tidsbanken_api.Dtos.Request
{
    public class RequestUpdateDto
    {
        /// <summary>
        /// The title of the request
        /// </summary>
        [StringLength(50)]
        public string Title { get; set; } = null;
        /// <summary>
        /// The start of the request period.
        /// </summary>
        public DateTime? PeriodStart { get; set; } = null;
        /// <summary>
        /// The end of the request period.
        /// </summary>
        public DateTime? PeriodEnd { get; set; } = null;
        /// <summary>
        /// The state of the request: Pending|Approved|Denied
        /// </summary>
        public string State { get; set; } = null;
    }
}
