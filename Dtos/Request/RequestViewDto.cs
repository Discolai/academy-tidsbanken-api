﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Request
{
    public class RequestViewDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public string State { get; set; }
        public string OwnerId { get; set; }
        public string OwnerName { get; set; }
        public DateTime? ModeratedAt { get; set; }
        public string ModeratorId { get; set; }
        public string ModeratorName { get; set; }
    }
}
