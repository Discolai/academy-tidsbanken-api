﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Comment
{
    public class CommentViewDto
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public int RequestId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }
}
