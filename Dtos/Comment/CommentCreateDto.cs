﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Comment
{
    public class CommentCreateDto
    {
        /// <summary>
        /// The comment itself
        /// </summary>
        [StringLength(512, MinimumLength = 1)]
        public string Message { get; set; }
    }
}
