﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Comment
{
    public class CommentQueryParams
    {
        /// <summary>
        /// Default 0
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Default 10
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Limit { get; set; } = 10;
    }
}
