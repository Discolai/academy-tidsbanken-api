﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.IneligiblePeriods
{
    public class IneligiblePeriodViewDto
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}
