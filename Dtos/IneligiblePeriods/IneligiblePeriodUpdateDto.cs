﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.IneligiblePeriods
{
    public class IneligiblePeriodUpdateDto
    {
        /// <summary>
        /// The start of the ineligible period.
        /// </summary>
        public DateTime? Start { get; set; } = null;
        /// <summary>
        /// The end of the ineligible period.
        /// </summary>
        public DateTime? End { get; set; } = null;
    }
}
