﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.IneligiblePeriods
{
    public class IneligiblePeriodCreateDto
    {
        /// <summary>
        /// The start of the ineligible period.
        /// </summary>
        [Required]
        public DateTime? Start { get; set; }
        /// <summary>
        /// The end of the ineligible period.
        /// </summary>
        [Required]
        public DateTime? End { get; set; }
    }
}
