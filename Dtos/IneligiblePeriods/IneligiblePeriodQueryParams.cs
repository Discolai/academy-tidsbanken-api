﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.IneligiblePeriods
{
    public class IneligiblePeriodQueryParams
    {
        /// <summary>
        /// Query periods from
        /// </summary>
        public DateTime Start { get; set; } = DateTime.MinValue;
        /// <summary>
        /// Query periods to
        /// </summary>
        public DateTime End { get; set; } = DateTime.MaxValue;
    }
}
