﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Auth
{
    public class LoginDto
    {
        [Required]
        [RegularExpression("password|refresh_token", 
            ErrorMessage = "Please specify a valid grant type, 'password' or 'refresh_token'")]
        public string GrantType { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RefreshToken { get; set; }
    }
}
