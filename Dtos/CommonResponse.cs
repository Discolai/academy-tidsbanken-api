﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;

namespace academy_tidsbanken_api.Dtos
{
    /// <summary>
    /// The common return type of all endpoints
    /// </summary>
    public class CommonResponse
    {
        /// <summary>
        /// The status of the response
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// A short descriptive message of the response
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// An optional general object with endpoint specific data
        /// </summary>
        public object Data { get; set; } = null;
        /// <summary>
        /// An optional object with error messages
        /// </summary>
        public IDictionary<string, string[]> Errors { get; set; } = null;

        public CommonResponse(int status = 200) 
        {
            Status = status;
            Message = ReasonPhrases.GetReasonPhrase(status);
        }
        public CommonResponse(ValidationProblemDetails problemDetails, int status = 200)
        {
            Status = status;
            Message = ReasonPhrases.GetReasonPhrase(status);
            Errors = problemDetails?.Errors;
        }
        public CommonResponse(ModelStateDictionary modelState, int status = 200) : this(new ValidationProblemDetails(modelState), status) { }

        public static ObjectResult Create(object data = null, int status = 200)
        {
            return new ObjectResult(new CommonResponse(status) { Data = data }) { StatusCode = status };
        }

        public static ObjectResult WithErrors(object data = null, IDictionary<string, string[]> errors = null, int status = 400)
        {
            return new ObjectResult(new CommonResponse(status) { Data = data, Errors = errors }) { StatusCode = status };
        }

        public static ObjectResult WithModelState(object data = null, ModelStateDictionary modelState = null, int status = 400)
        {
            return new ObjectResult(new CommonResponse(modelState, status) { Data = data }) { StatusCode = status };
        }

    }
}
