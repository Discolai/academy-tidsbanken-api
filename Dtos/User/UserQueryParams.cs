﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.User
{
    public class UserQueryParams
    {
        [Range(0, int.MaxValue)]
        public int Offset { get; set; } = 0;
        [Range(1, int.MaxValue)]
        public int Limit { get; set; } = 50;
    }
}
