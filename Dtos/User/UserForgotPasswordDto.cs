﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.User
{
    public class UserForgotPasswordDto
    {
        [StringLength(320)]
        [EmailAddress]
        [Required]
        public string Email { get; set; }
    }
}
