﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.User
{
    public class UserLimitedViewDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }

    }
}
