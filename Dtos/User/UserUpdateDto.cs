﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.User
{
    public class UserUpdateDto
    {
        public bool? IsAdmin { get; set; } = null;

        [EmailAddress]
        [StringLength(320)]
        public string Email { get; set; } = null;

        /// <summary>
        /// Picture url
        /// </summary>
        [StringLength(60)]
        public string Picture { get; set; } = null;

        [StringLength(50)]
        public string FirstName { get; set; } = null;

        [StringLength(50)]
        public string LastName { get; set; } = null;
    }
}
