﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.User
{
    public class UserResetPasswordDto
    {
        [Required]
        public string Password1 { get; set; }
        [Required]
        public string Password2 { get; set; }
    }
}
