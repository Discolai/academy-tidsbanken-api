﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Services;

namespace academy_tidsbanken_api.Dtos.Settings
{
    public class SettingsUpdateDto
    {
        public int Id { get; } = SettingsService.SettingsId;

        /// <summary>
        /// The max vacation length in business days
        /// </summary>
        [Range(1, int.MaxValue)]
        public int? MaxVacationLength { get; set; } = null;
    }
}
