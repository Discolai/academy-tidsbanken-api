﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace academy_tidsbanken_api.Dtos.Settings
{
    public class SettingsViewDto
    {
        public int MaxVacationLength { get; set; }
    }
}
