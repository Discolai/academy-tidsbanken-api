﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace academy_tidsbanken_api.Data
{
    public class TidsbankenApiDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public DbSet<Setting> Settings { get; set; }
        public DbSet<IneligiblePeriod> IneligiblePeriods { get; set; }

        public Task<Setting> SettingsRow
        {
            get
            {
                return Settings.FirstOrDefaultAsync();
            }
        }



        public TidsbankenApiDbContext(DbContextOptions<TidsbankenApiDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Setting>().HasData(
                new Setting
                {
                    Id = 1,
                    MaxVacationLength = 10
                }
            );

            // Set user to not cascade since request already cascades on user
            modelBuilder.Entity<Comment>()
                .HasOne<User>(c => c.User)
                .WithMany(u => u.Comments)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(c => c.UserId);


            modelBuilder.Entity<Request>()
                .HasOne<User>(r => r.Owner)
                .WithMany(u => u.SubmittedRequests)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(r => r.OwnerId)
                .IsRequired(true);
            modelBuilder.Entity<Request>()
                .HasOne<User>(r => r.Moderator)
                .WithMany(m => m.ModeratedRequests)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(false)
                .HasForeignKey(r => r.ModeratorId);
        }
    }
}
