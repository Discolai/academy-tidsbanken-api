﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Extensions;

namespace academy_tidsbanken_api.Services
{
    public class OIDSettings
    {

        public string Authority { get; set; }
        public string AuthorityAdmin { get; set; } 
        public string AccessAudience { get; set; }
        public string IdAudience { get; set; }
        public string ClientSecret { get; set; }

        public string TokenEndpoint 
        { 
            get {
                var uri = new Uri(Authority);
                return uri.WithRelative("/protocol/openid-connect/token");
            } 
        }
    }
}
