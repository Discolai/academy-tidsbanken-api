﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using academy_tidsbanken_api.Models.OID;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Services
{
    public class OIDServiceAccount
    {
        private readonly HttpClient _httpClient;
        private readonly OIDSettings _oIDSettings;
        private readonly ILogger<OIDServiceAccount> _logger;
        private readonly JwtSecurityTokenHandler TokenHandler = new JwtSecurityTokenHandler();

        public string EncodedToken { get; private set; } = null;

        public JwtSecurityToken Token { get; private set; } = null;

        public OIDServiceAccount(HttpClient httpClient, OIDSettings oIDSettings, ILogger<OIDServiceAccount> logger)
        {
            _httpClient = httpClient;
            _oIDSettings = oIDSettings;
            _logger = logger;
        }

        public async Task<AuthenticationHeaderValue> GetAuthorizationHeaderAsync()
        {
            if (Token == null || Token.ValidTo < DateTime.UtcNow)
            {
                if (!await LoginAsync()) return null;
            }
            return new AuthenticationHeaderValue("bearer", EncodedToken);
        }

        public async Task<bool> LoginAsync()
        {
            var parameters = new Dictionary<string, string>
            {
                { "client_id", _oIDSettings.IdAudience },
                { "client_secret", _oIDSettings.ClientSecret },
                { "grant_type", GrantTypes.ClientCredentials },
                { "scope", "openid" }
            };
            var request = new HttpRequestMessage(HttpMethod.Post, _oIDSettings.TokenEndpoint)
            {
                Content = new FormUrlEncodedContent(parameters)
            };
            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed logging in service user at {} UTC", nameof(LoginAsync), DateTime.UtcNow);
                return false;
            }

            OIDTokens tokens;
            try
            {

                tokens = await JsonSerializer.DeserializeAsync<OIDTokens>(await response.Content.ReadAsStreamAsync());
                EncodedToken = tokens.access_token;
                Token = TokenHandler.ReadJwtToken(tokens.access_token);
                _logger.LogInformation("{}: OID service account fetched a new token at {} UTC", nameof(LoginAsync), DateTime.UtcNow);
                return true;
            }
            catch (JsonException)
            {
                _logger.LogError("{}: Failed parsing Json response from Authority at {} UTC", nameof(LoginAsync), DateTime.UtcNow);
                return false;
            }

        }
    }
}
