﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using academy_tidsbanken_api.Dtos.Auth;
using academy_tidsbanken_api.Dtos.User;
using academy_tidsbanken_api.Extensions;
using academy_tidsbanken_api.Models;
using academy_tidsbanken_api.Models.OID;
using AutoMapper;
using AutoMapper.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace academy_tidsbanken_api.Services
{
    public static class GrantTypes
    {
        public const string Password = "password";
        public const string Token = "refresh_token";
        public const string ClientCredentials = "client_credentials";
    }

    public static class EmailActions
    {
        public const string VerifyEmail = "VERIFY_EMAIL";
        public const string UpdatePassword = "UPDATE_PASSWORD";
    }


    public class OIDService
    {
        private readonly HttpContext _httpContext;
        private readonly HttpClient _httpClient;
        private readonly OIDSettings _oIDSettings;
        private readonly OIDServiceAccount _oIDServiceAccount;
        private readonly ILogger<OIDService> _logger;
        private readonly IMapper _mapper;

        private Dictionary<string, string> BaseParameters 
        { 
            get
            {
                return new Dictionary<string, string>
                {
                    { "client_id", _oIDSettings.IdAudience },
                    { "client_secret", _oIDSettings.ClientSecret },
                    { "scope", "openid" }
                };
            }
                
        }

        public OIDService(
            IHttpContextAccessor httpContextAccessor, 
            HttpClient httpClient, 
            OIDSettings oIDSettings, 
            ILogger<OIDService> logger, 
            IMapper mapper,
            OIDServiceAccount oIDServiceAccount)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _httpClient = httpClient;
            _oIDSettings = oIDSettings;
            _logger = logger;
            _mapper = mapper;
            _oIDServiceAccount = oIDServiceAccount;
        }

        public async Task<OIDTokens> LoginAsync(LoginDto loginData)
        {
            var parameters = BaseParameters;
            parameters.Add("grant_type", loginData.GrantType);

            if (loginData.GrantType == GrantTypes.Password)
            {
                parameters.Add("username", loginData.Username);
                parameters.Add("password", loginData.Password);
            }
            else if (loginData.GrantType == GrantTypes.Token)
            {
                parameters.Add("refresh_token", loginData.RefreshToken);
            }
            else
            {
                _logger.LogError("{}: Invalid grant type {} at {} UTC", nameof(LoginAsync), loginData.GrantType, DateTime.UtcNow);
                return null;
            }

            var request = new HttpRequestMessage(HttpMethod.Post, _oIDSettings.TokenEndpoint)
            {
                Content = new FormUrlEncodedContent(parameters)
            };

            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed logging in user at {} UTC", nameof(LoginAsync), DateTime.UtcNow);
                return null;
            }

            try
            {
                return await JsonSerializer.DeserializeAsync<OIDTokens>(await response.Content.ReadAsStreamAsync()); ;
            }
            catch (JsonException)
            {
                _logger.LogError("{}: Failed parsing Json response from Authority at {} UTC", nameof(LoginAsync), DateTime.UtcNow);
                return null;
            }
        }

        private async Task<bool> HandleRemoteUserAdminActionAsync(string userId, HttpMethod action)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return false;

            // Send a request asking for the id of the admin role
            var roleRequest = new HttpRequestMessage(HttpMethod.Get, new Uri(_oIDSettings.AuthorityAdmin).WithRelative("/roles/admin"));
            roleRequest.Headers.Authorization = authorization;

            var roleRequestResponse = await _httpClient.SendAsync(roleRequest);
            if (!roleRequestResponse.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed getting admin role at {} UTC", nameof(HandleRemoteUserAdminActionAsync), DateTime.UtcNow);
                return false;
            }

            // Send another request assigning the admin role to the provided user
            var roleSetRequest = new HttpRequestMessage(action, new Uri(_oIDSettings.AuthorityAdmin).WithRelative($"/users/{userId}/role-mappings/realm"))
            {
                // The request expects a list of roles, so we insert the json object into a list
                Content = new StringContent($"[{await roleRequestResponse.Content.ReadAsStringAsync()}]", Encoding.UTF8, "application/json")
            };
            roleSetRequest.Headers.Authorization = authorization;


            var roleSetRequestResponse = await _httpClient.SendAsync(roleSetRequest);
            if (!roleSetRequestResponse.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed setting or removing admin role at {} UTC", nameof(HandleRemoteUserAdminActionAsync), DateTime.UtcNow);
                return false;
            }
            return true;
        }

        public async Task<bool> AddRemoteUserAdminAsync(string userId)
        {
            return await HandleRemoteUserAdminActionAsync(userId, HttpMethod.Post);
        }

        public async Task<bool> RemoveRemoteUserAdminAsync(string userId)
        {
            return await HandleRemoteUserAdminActionAsync(userId, HttpMethod.Delete);
        }

        public async Task<bool> RemoteExecuteActionsEmailAsync(string userId, params string[] actions)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return false;

            var request = new HttpRequestMessage(HttpMethod.Put, new Uri(_oIDSettings.AuthorityAdmin).WithRelative($"/users/{userId}/execute-actions-email"))
            {
                Content = new StringContent(JsonSerializer.Serialize(actions), Encoding.UTF8, "application/json")
            };
            request.Headers.Authorization = authorization;

            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed to execute email actions at {} UTC", nameof(RemoteExecuteActionsEmailAsync), DateTime.UtcNow);
                return false;
            }
            return true;
        }

        public async Task<User> CreateRemoteUser(UserCreateDto createDto)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return null;

            var parameters = new Dictionary<string, object>
            {
                { "firstName", createDto.FirstName },
                { "lastName", createDto.LastName },
                { "email", createDto.Email },
                { "username", createDto.Email },
                { "enabled", true },
                { 
                    "requiredActions", 
                    new string[]
                    {
                        "UPDATE_PASSWORD",
                        "VERIFY_EMAIL"
                    }
                }
            };

            // Send a request to create a new user
            var userCreateRequest = new HttpRequestMessage(HttpMethod.Post, new Uri(_oIDSettings.AuthorityAdmin).WithRelative("/users"))
            {
                Content = new StringContent(JsonSerializer.Serialize(parameters), Encoding.UTF8, "application/json")
            };
            userCreateRequest.Headers.Authorization = authorization;

            var userCreateResponse = await _httpClient.SendAsync(userCreateRequest);
            if (!userCreateResponse.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed creating remote user at {} UTC", nameof(CreateRemoteUser), DateTime.UtcNow);
                return null;
            }

            // Create a user object from the newly created user
            var user = _mapper.Map<User>(createDto);
            user.Id = userCreateResponse.Headers.Location.AbsoluteUri.Split("/").Last();

            await RemoteExecuteActionsEmailAsync(user.Id, EmailActions.VerifyEmail, EmailActions.UpdatePassword);

            // Do another remote call to set the user as admin if specified
            if (createDto.IsAdmin && !await AddRemoteUserAdminAsync(user.Id))
            {
                _logger.LogError("{}: Failed setting remote user as admin at {} UTC", nameof(CreateRemoteUser), DateTime.UtcNow);
                user.IsAdmin = false;
                return user;
            }

            return user;
        }

        public async Task<bool> UpdateRemoteUserAsync(string userId, UserUpdateDto updateDto)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return false;

            var parameters = new Dictionary<string, string>();
            if (updateDto.FirstName != null) parameters.Add("firstName", updateDto.FirstName);
            if (updateDto.LastName != null) parameters.Add("lastName", updateDto.LastName);
            if (updateDto.Email != null)
            {
                parameters.Add("email", updateDto.Email);
                parameters.Add("username", updateDto.Email);
            }

            if (parameters.Count > 0)
            {
                // Send a request to update remote user
                var userUpdateRequest = new HttpRequestMessage(HttpMethod.Put, new Uri(_oIDSettings.AuthorityAdmin).WithRelative($"/users/{userId}"))
                {
                    Content = new StringContent(JsonSerializer.Serialize(parameters), Encoding.UTF8, "application/json")
                };
                userUpdateRequest.Headers.Authorization = authorization;

                var response = await _httpClient.SendAsync(userUpdateRequest);
                if (!response.IsSuccessStatusCode)
                {
                    _logger.LogError("{}: Failed updating remote user at {} UTC", nameof(UpdateRemoteUserAsync), DateTime.UtcNow);
                    return false;
                }
            }

            if (updateDto.IsAdmin != null)
            {
                return updateDto.IsAdmin switch
                {
                    true => await AddRemoteUserAdminAsync(userId),
                    false => await RemoveRemoteUserAdminAsync(userId),
                };
            }
            return true;
        }

        public async Task<bool> DeleteRemoteUserAsync(string userId)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return false;

            var request = new HttpRequestMessage(HttpMethod.Delete, new Uri(_oIDSettings.AuthorityAdmin).WithRelative($"/users/{userId}"));
            request.Headers.Authorization = authorization;

            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed deleting remote user at {} UTC", nameof(DeleteRemoteUserAsync), DateTime.UtcNow);
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateRemoteUserPassword(string userId, string password)
        {
            var authorization = await _oIDServiceAccount.GetAuthorizationHeaderAsync();
            if (authorization == null) return false;

            var parameters = new Dictionary<string, object>
            {
                { "type", "password" },
                { "temporary", false },
                { "value", password }
            };

            var request = new HttpRequestMessage(HttpMethod.Put, new Uri(_oIDSettings.AuthorityAdmin).WithRelative($"/users/{userId}/reset-password"))
            {
                Content = new StringContent(JsonSerializer.Serialize(parameters), Encoding.UTF8, "application/json")

            };
            request.Headers.Authorization = authorization;

            var response = await _httpClient.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                _logger.LogError("{}: Failed updating remote user password {} UTC", nameof(UpdateRemoteUserPassword), DateTime.UtcNow);
                return false;
            }
            return true;
        }
    }
}
