﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos.User;
using academy_tidsbanken_api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Services
{
    public class UserService
    {
        private readonly ClaimsPrincipal _user;
        private readonly TidsbankenApiDbContext _context;

        public string CurrentUserId { get => _user.FindFirstValue(ClaimTypes.NameIdentifier); }
        public bool CurrentUserIsAdmin { get => _user.IsInRole("admin"); }
        public string CurrentUserEmail { get => _user.FindFirstValue("email"); }

        public UserService(IHttpContextAccessor httpContextAccessor, TidsbankenApiDbContext context)
        {
            _user = httpContextAccessor.HttpContext.User;
            _context = context;
        }

        public async Task<User> CurrentUserAsync()
        {
            return await _context.Users.SingleOrDefaultAsync(u => u.Id == _user.FindFirstValue(ClaimTypes.NameIdentifier));
        }

        public async Task<IEnumerable<User>> GetAllAsync(UserQueryParams parameters)
        {
            return await _context.Users.Skip(parameters.Offset).Take(parameters.Limit).ToArrayAsync();
        }

        public async Task<User> UpdateAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> DeleteById(string userId)
        {
            var user = await GetByIdAsync(userId);
            if (user == null) return null;

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<User> GetByEmailAsync(string email) =>  await _context.Users.SingleOrDefaultAsync(u => u.Email == email);
        public async Task<User> GetByIdAsync(string userId) => await _context.Users.SingleOrDefaultAsync(u => u.Id == userId);

        public async Task SaveUserAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }
    }
}
