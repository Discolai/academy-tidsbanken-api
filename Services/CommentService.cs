﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos.Comment;
using academy_tidsbanken_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Services
{
    public class CommentService
    {
        private readonly TidsbankenApiDbContext _context;

        public CommentService(TidsbankenApiDbContext context)
        {
            _context = context;
        }

        private IIncludableQueryable<Comment, User> BaseQuery(int offset, int limit)
        {
            return _context.Comments.Skip(offset).Take(limit).OrderByDescending(r => r.CreatedOn.Date).ThenBy(r => r.CreatedOn.TimeOfDay).Include(c => c.User);
        }

        public async Task<IEnumerable<Comment>> GetByRequestIdAsync(int requestId, int offset, int limit)
        {
            return await BaseQuery(offset, limit).Where(c => c.RequestId == requestId).ToArrayAsync();
        }

        public async Task<Comment> GetByRequestAndCommentIdAsync(int requestId, int commentId)
        {
            return await _context.Comments.Include(c => c.User).SingleOrDefaultAsync(c => c.RequestId == requestId && c.Id == commentId);
        }

        public async Task SaveAsync(Comment comment)
        {
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
        }

        public async Task<Comment> UpdateAsync(Comment comment)
        {
            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return comment;
        }

        public async Task DeleteAsync(Comment comment)
        {
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
        }
    }
}
