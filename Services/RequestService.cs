﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos.Request;
using academy_tidsbanken_api.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Services
{
    public class RequestService
    {
        private readonly TidsbankenApiDbContext _context;
        private readonly ILogger<RequestService> _logger;
        private readonly IMapper _mapper;

        public RequestService(TidsbankenApiDbContext context, ILogger<RequestService> logger, IMapper mapper)
        {
            _context = context;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<Request> GetByIdWithCommentsAsync(int requestId)
        {
            return await _context.Requests.Include(r => r.Owner).Include(r => r.Moderator).Include(r => r.Comments).ThenInclude(c => c.User).SingleOrDefaultAsync(r => r.Id == requestId);
        }

        public async Task<Request> GetByIdAsync(int requestId)
        {
            return await _context.Requests.Include(r => r.Owner).Include(r => r.Moderator).SingleOrDefaultAsync(r => r.Id == requestId);
        }

        private IQueryable<Request> BaseQuery(RequestQueryParams parameters)
        {
            return _context.Requests
                .Skip(parameters.Offset)
                .Take(parameters.Limit)
                .OrderByDescending(r => r.PeriodStart.HasValue ? r.PeriodStart.Value.Date : new DateTime())
                .Include(r => r.Owner)
                .Include(r => r.Moderator)
                .Where(r => (r.PeriodStart >= parameters.Start && r.PeriodStart <= parameters.End) || (r.PeriodEnd >= parameters.Start && r.PeriodEnd <= parameters.End));
        }

        public async Task<IEnumerable<Request>> GetAllAsync(RequestQueryParams parameters, string state = null)
        {
            if (!string.IsNullOrEmpty(state))
            {
                if (!RequestStates.IsValid(state))
                {
                    _logger.LogError("{}: Invalid request state {} at {} UTC", nameof(GetAllAsync), state, DateTime.UtcNow);
                    return null;
                }
                return await BaseQuery(parameters).Where(r => r.State == state).ToArrayAsync();
            }
            return await BaseQuery(parameters).ToArrayAsync();
        }

        public async Task<IEnumerable<Request>> GetAllLimitByUser(string userId, RequestQueryParams parameters)
        {
            return await BaseQuery(parameters).Where(r => r.Owner.Id == userId || r.State == RequestStates.Approved).ToArrayAsync();
        }

        public async Task<IEnumerable<Request>> GetByOwnerAsync(RequestQueryParams parameters, User owner, string state = null)
        {
            if (!string.IsNullOrEmpty(state))
            {
                if (!RequestStates.IsValid(state))
                {
                    _logger.LogError("{}: Invalid request state {} at {} UTC", nameof(GetByOwnerAsync), state, DateTime.UtcNow);
                    return null;
                }
                return await BaseQuery(parameters).Where(r => r.OwnerId == owner.Id && r.State == state).ToArrayAsync();

            }
            return await BaseQuery(parameters).Where(r => r.OwnerId == owner.Id).ToArrayAsync();
        }

        public async Task<Request> CreateAsync(Request request)
        {
            await _context.Requests.AddAsync(request);
            await _context.SaveChangesAsync();
            return request;
        }

        public async Task<Request> UpdateAsync(Request request, User moderator = null)
        {
            if (moderator != null)
            {
                request.ModeratedAt = DateTime.UtcNow;
                request.Moderator = moderator;
            }

            _context.Entry(request).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return request;
        }

        public async Task DeleteAsync(Request request)
        {
            _context.Requests.Remove(request);
            await _context.SaveChangesAsync();
        }

    }
}
