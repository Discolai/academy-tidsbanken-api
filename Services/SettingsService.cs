﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos.Settings;
using academy_tidsbanken_api.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace academy_tidsbanken_api.Services
{
    public class SettingsService
    {
        public const int SettingsId = 1;

        private Setting _settings = null;

        private readonly TidsbankenApiDbContext _context;
        private readonly IMapper _mapper;

        public SettingsService(TidsbankenApiDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<Setting> GetAsync()
        {
            if (_settings == null)
            {
                _settings = await _context.Settings.FirstOrDefaultAsync();
            }
            return _settings;
        }

        public async Task<Setting> UpateSettingsAsync(SettingsUpdateDto updateDto)
        { 
            var settings = _mapper.Map(updateDto, await GetAsync());
            _context.Entry(settings).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return settings;
        }

    }
}
