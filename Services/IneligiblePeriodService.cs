﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using academy_tidsbanken_api.Data;
using academy_tidsbanken_api.Dtos.IneligiblePeriods;
using academy_tidsbanken_api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace academy_tidsbanken_api.Services
{
    public class IneligiblePeriodService
    {
        private readonly TidsbankenApiDbContext _context;

        public IneligiblePeriodService(TidsbankenApiDbContext context)
        {
            _context = context;
        }

        private IQueryable<IneligiblePeriod> BaseQuery(IneligiblePeriodQueryParams parameters)
        {
            return _context.IneligiblePeriods
                .OrderByDescending(ip => ip.Start.HasValue ? ip.Start.Value.Date : new DateTime())
                .Include(ip => ip.User)
                .Where(ip => (ip.Start >= parameters.Start && ip.Start <= parameters.End) || (ip.End >= parameters.Start && ip.End <= parameters.End));
        }

        public async Task<IneligiblePeriod> GetByIdAsync(int ipId)
        {
            return await _context.IneligiblePeriods.SingleOrDefaultAsync(ip => ip.Id == ipId);
        }

        public async Task<IEnumerable<IneligiblePeriod>> GetAllAsync(IneligiblePeriodQueryParams parameters)
        {
            return await BaseQuery(parameters).ToArrayAsync();
        }

        public async Task<IneligiblePeriod> CreateAsync(IneligiblePeriod ip)
        {
            await _context.IneligiblePeriods.AddAsync(ip);
            await _context.SaveChangesAsync();
            return ip;
        }

        public async Task<IneligiblePeriod> UpdateAsync(IneligiblePeriod ip)
        {
            _context.Entry(ip).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return ip;
        }

        public async Task DeleteAsync(IneligiblePeriod ip)
        {
            _context.IneligiblePeriods.Remove(ip);
            await _context.SaveChangesAsync();
        }
    }
}
