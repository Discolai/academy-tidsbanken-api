# Tidsbanken case api

## Team members
- Nikolai Norum Hansen
- Felix Grimsrud
- Sondre Kindem

## Installation
### Variables
Key | Value
--- | ---
OID_AcessAudience | The audience for the access token
OID_Authority | Link to the keycloak realm
OID_AuthorityAdmin | Link to the admin rest api
OID_ClientSecret | The client secret for the keycloak realms client
OID_IdAudience | The audience for the id token
### Dotnet cli
- Install required packages $```dotnet restore```
- Run the development server $```dotnet run```

### Docker
- Build the docker image $```docker build -t tidsbanken_api .```
- Run the image $```docker run -d -p 8080:80 --name tidsbanken_api tidsbanken_api```

## Documentation
You can read the swagger documentation from the live api on [azure](https://tidsbanken-api.azurewebsites.net/swagger/index.html)
or as a [pdf](./docs/documentation.pdf)